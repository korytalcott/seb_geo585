import numpy as np


class SurfaceEnergyBalance(object):

    #  *Static contstants!!*
    # These are defined outside the constructor __init__
    # and belong to the class. No copy of static variables is 
    # made when you create an object, all objects share the same copy of 
    # static variables
    P = 101.3    #  kPa
    rho_w = 1000 #  kg/m3
    rho_a = 1.29 #  kg/m3
    heat_cap_water = 0.002102 #  MJKg-1K-1
    heat_cap_air = 0.00101    #  MJKg-1K-1
    latent_heat_fus = 0.334   #  MJKg-1
    latent_heat_vapor = 2.47  #  MJKg-1
    stef_boltz = 4.9e-09      #  MJm-2d-1K-4
    von_karman = 0.4          # no units
    

    def __init__(self, albedo, surf_emissivity, surf_rough_z0, surf_zeroplane_zd, 
                 surf_measurem_za, soil_temp_dmpdpth, soil_dmptdpth, soil_thermal_cond,
                 atmosph_emissivity):
        
        """
        Parameters
        ----------
        albedo : (aka alpha) albedo (dimensionless)
        
        surf_emissivity : (aka epsilon_s) surface emissivity (dimensionless)
        
        surf_rough_z0 : (aka z_0) surface roughness height (m)
        
        surf_zeroplane_zd : (aka z_d) surface zero-plane displacement (m)
        
        surf_measurem_za : (aka z_m) elevation (with respect to soil surface) of the widspeed (m)

        soil_temp_dmpdpth : (aka T_dd) temperature at the damping depth (degC)
        
        soil_dmptdpth : (aka z_dd) damping depth (m)
        
        soil_thermal_cond : (aka K_T) soil thermal conductivity (MJm^-1d^-1C^-1)
            
        atmosph_emissivity : emissivity of the atmosphere (unitless)
        
        Returns
        -------
        None."""
        
        self.albedo = albedo
        
        self.surf_emissivity = surf_emissivity
        
        self.surf_rough_z0 = surf_rough_z0
        
        self.surf_zeroplane_zd = surf_zeroplane_zd 
        
        self.surf_measurem_za = surf_measurem_za
        
        self.soil_temp_dmpdpth = soil_temp_dmpdpth
        
        self.soil_dmptdpth = soil_dmptdpth
        
        self.soil_thermal_cond = soil_thermal_cond
        
        self.atmosph_emissivity = atmosph_emissivity
        
        ### Create a list to store skin temperatures in
        self.skin_temp = []
        self.net_radiation = []
        self.latent_heat = []
        self.sensible_heat = []
        self.ground_heat = []
        self.time_stamps = []
        self.energy_balance_errors = []

    def _net_radiation(self, S, T_s, Ldown):
        """returns net radiation with given S, Ts and Ldown 
        
        :input/boundary condition S:  Solar constant from Sun to surface MJm-2d-1
        :state variable T_s:  soil skin Temp in degrees Celsius
        :input/boundary condition Ldown: longwave radiation from the atmosphere MJm-2d-1
        :param albedo: surface albedo, no units
        :param surf_emissivity: surface emissivity, no units
        :param stef_boltz: Steffan_Bolztmann constant, MJm-2d-1K-4
        :returns: net radiation, MJm-2d-1
        """
        
        return (1 - self.albedo) * S + Ldown - self.surf_emissivity * self.stef_boltz * (T_s + 273.2)**4 - (1 - self.surf_emissivity) * Ldown
    
    def _dnet_radiation (self, S, T_s, Ldown):
        """returns the derivative of net radiation with respect to T_s with given S, Ts and Ldown 
        
        :input/boundary condition S:  Solar constant from Sun to surface MJm-2d-1
        :state variable Ts:  soil skin Temp in degrees Celsius
        :input/boundary condition Ldown: longwave radiation from the atmosphere MJm-2d-1
        :param albedo: surface albedo, no units
        :param surf_emissivity: surface emissivity, no units
        :param stef_boltz: Steffan_Bolztmann constant, MJm-2d-1K-4
        :returns: derivative net radiation
        """
        
        return -4 * self.surf_emissivity * self.stef_boltz * (T_s + 273.2)**3
        
        

    def _latent_heat(self, K_a, e_a, e_s):
        '''Returns Latent heat flux (dependent on differencd between vapor pressure in air and soil.
            State variables (input by user):
            :param K_a: Aerodynamic conductivity (m/d)
            :param e_a: Actual vapor pressure of the air (kPa)
            :param e_s: Actual vapor pressure of the soil (kPa)
            
            Returns latent heat in (MJ/m2 d)
            '''
        
        L_E = self.latent_heat_vapor * 0.622 * self.rho_a / self.P * K_a * (e_a - e_s)
        return L_E

    def _VP(self, t, RH):
        """air vapor pressure function

        :input t: temperature of air/soil (celsius)
        :input  airRH: relative humidity of air/soil (unitless ratio)

        :return: Actual Vapor Pressure(kPa)
        """

        solution = 0.611 * np.exp((17.3 * t) / (t + 237.3)) * RH
        return solution

    def _sensible_heat(self, K_a, T_a, T_s):
        """calculate sensible heat flux L_H with associated aerodynamic conductivity factor
        
        :param K_a: aerodynamic conductivity factor Ka [md-1]
        :param T_a: air temperature [deg C]
        :param T_s: soil skin temperature [deg C]
        :returns: sensible heat flux L_H [MJm-2d-1]
        """
        
        L_H = self.heat_cap_air * self.rho_a * K_a * (T_a-T_s) 
        return L_H
        
    def _dsensible_heat(self, K_a, T_a, T_s):
        ''' 
        :param K_a: aerodynamic conductivity factor Ka [md-1]
        :param T_a: air temperature [deg C]
        :param T_s: soil skin temperature [deg C]
        :returns: dLH(T_s)/d(T_s)
        '''
        return -self.rho_a*self.heat_cap_air*K_a
        
    def _groundheat(self, Ts):
        """returns ground heat flux with given Ts

        :state variable Ts:  soil skin temp (C)
        :return: groundheat heat flux(MJm-2d-1)
        """
        return self.soil_thermal_cond * (self.soil_temp_dmpdpth - Ts) / self.soil_dmptdpth

    def _dgroundheat(self):
        """

        :return: d/dTs(groundheat)
        """
        return -1 * self.soil_thermal_cond / self.soil_dmptdpth

    def _K_a(self, wind_speed):
        '''
        :param u: wind speed (ms-1)
        returns aerodynamic conductivity (m/d)
        '''
        von_karman=0.4
        self.wind_speed = wind_speed*60*60*24 #conversion from ms-1 to md-1
        import math
        return ((von_karman**2)*self.wind_speed)/(math.log(((self.surf_measurem_za-self.surf_zeroplane_zd)/self.surf_rough_z0))**2)
    
    def _dlatent_heat(self, Ts, K_a, RH):
        '''Returns the derivative for latent heat with respect to soil temp (Ts). 
        :param K_a: Aerodynamic conductivity (m/d)
        :param Ts: Temperature of soil (degrees C)
        :param RH: Relative humidity of soil (no units)
        '''
        desdTs = self._dVP(Ts, RH)
        dLdTs = -1 * self.latent_heat_vapor * (0.622 * self.rho_a / self.P) * K_a * desdTs
        return dLdTs

    def _solve_energy_balance(self, S, Ldown, AirTemp, windspeed, airRH, soilRH):
        """
        Augments skin_temp array with solution of soil surface energy balance

        :param S: Incoming solar radiation, array or scalar [MJm-2d-1]
        :param Ldown: Incoming longwave radiation, array or scalar [MJm-2d-1]
        :param AirTemp: Air temperature, array or scalar [deg C]
        :param windspeed: Horizontal component of wind speed, array or scalar [m/s]
        :param airRH: Relative humidity of air, array or scalar [-]
        :param soilRH: Relative humidity of soil pores, array or scalar [-]
        :return: returns the energy balance closure error, array or scalar [MJm-2d-1]
        """

        oldTs = 0
        if len(self.skin_temp) > 0:
            oldTs = self.skin_temp[-1]
        #  Initiates temperature with last solution of skin temperature if there is any, otherwise 0
        Ts = oldTs

        energy_balance_err = 1

        # Aerodynamic conductance does nto depend on Ts, so it can be calculated outside the
        # Newton-Rapshon loop
        Ka = self._K_a(windspeed)

        i = 0
        # Start Newton-Raphson loop. Stop when energy balance error is smaller than 1e-6
        # or number of iterations exceeds 100
        while (abs(energy_balance_err) > 1e-6) and (i < 500):

            ea = self._VP(AirTemp, airRH)
            es = self._VP(Ts, soilRH)
            energy_balance_err = (self._net_radiation(S, Ts, Ldown)
                                  + self._sensible_heat(Ka, AirTemp, Ts)
                                  + self._latent_heat(Ka, ea, es)
                                  + self._groundheat(Ts))
            denergy_balance_err = (self._dnet_radiation(S, Ts, Ldown)
                                  + self._dsensible_heat(Ka, AirTemp, Ts)
                                  + self._dlatent_heat(Ka, ea, es)
                                  + self._dgroundheat())
            # Newton Raphson method to update Ts
            Ts = Ts - energy_balance_err/denergy_balance_err # <--This is the line of code that does the work

            i += 1

        # append skin temperature solution and corresponding energy fluxes to internal variables
        self.skin_temp.append(Ts)
        self.net_radiation.append(self._net_radiation(S, Ts, Ldown))
        self.sensible_heat.append(self._sensible_heat(Ka, AirTemp, Ts))
        self.latent_heat.append(self._latent_heat(Ka, ea, es))
        self.ground_heat.append(self._groundheat(Ts))

        return energy_balance_err

    def run_energy_balance(self, fn_met_info):
        """
        Solution of the surface energy balance for the meteorological conditions provided by file ``fn_met_info``.
        File ``fn_met_info`` must be an ascii table with a row per time step and a column per relevant meteorological
        variable with the following column names:

        date: date of the time stape (mm/dd/yyy)

        Tavg: Average air temperature (deg C)

        wind: Wind speed (m/s)

        Air_RH: Air relative humidity (-)

        Soil_RH: Soil pores relative humidty (-)

        SW_Rad: Short wave radiation (MJ/m2/day)

        LW_Rad: Incoming long wave radiation (MJ/m2/day)


        :param fn_met_info: file name of ascii file with meteorological information
        :return: numpy array with soil skin temperature (degC), numpy array with energy balance error (MJ/m2/day)
        """


        import pandas as pd

        df_meteo_data = pd.read_table(fn_met_info, index_col=['date'])

        # iterate thru rows of pandas dataframe parse and run _solve_energy_balance
        for index, row in df_meteo_data.iterrows():

            Sdown = row['SW_Rad']
            Ldown = row['LW_Rad']
            AirTemp = row['Tavg']
            ws = row['wind']
            AirRH = row['Air_RH']
            SoilRH = row['Soil_RH']

            eb_err = self._solve_energy_balance(Sdown, Ldown, AirTemp, ws, AirRH, SoilRH)
            self.energy_balance_errors.append(eb_err)
            self.time_stamps.append(index)
        return np.array(self.skin_temp), np.array(self.energy_balance_errors)

    def _dVP(self, t, RH):
        """air vapor pressure function

        :input tair: temperature of air (celsius)
        :input  airRH: relative humidity of air (unitless ratio)

        :return: The Derivative of Actual Vapor Pressure (kPa)
        """
        
        #solution = 0.611* Soil_RH *((((Ts+237.3)*17.27-17.27*Ts)/(Ts+237.3)**2))*np.exp((17.27*Ts/(Ts+237.3)))
        
        solution = 0.611 * RH * (((t + 237.3) * 17.27 - 17.27 * t) / (t + 237.3) ** 2) * np.exp(
            (17.27 * t / (t + 237.3)))

        return solution



